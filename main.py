from random import randint

NUMBER_RELOCATION_ATTEMPTS = 100


def print_map(ships):
    title_numbers = [str(i) for i in range(1, 11)]
    print('   {:2}'.format(' '.join(title_numbers)))
    for y in range(1, 11):
        ships_on_this_line = []
        for x in range(1, 11):
            ships_on_this_line.append('#' if (x, y) in ships else '.')
        print('{:2} {}'.format(y, ' '.join(ships_on_this_line)))


def print_cords(ships):
    print('Координаты:')
    for index, ship in enumerate(ships):
        print('{:4} - (x: {:2}, y: {:2})'.format(index + 1, ship[0], ship[1]))


def place_ships(number_of_ships):
    ships = []
    for ship_number in range(number_of_ships):
        x = (ship_number % 5) * 2 + 1
        y = int(ship_number / 5) * 2 + 1
        ships.append((x, y))
    return ships


def place_ships_in_chessboard_order(number_of_ships):
    ships = []
    for ship_number in range(number_of_ships):
        x = (ship_number % 10) * 2 + 1
        y = int(ship_number / 10) * 2 + 1
        move_to_next_line = x > 10
        if move_to_next_line:
            x -= 9
            y += 1
        ships.append((x, y))
    return ships


def random_place_ships(number_of_ships, check_possibility_function):
    ships = []
    for ship_number in range(number_of_ships):
        is_placed = False
        for attempt_number in range(NUMBER_RELOCATION_ATTEMPTS):
            x = randint(1, 10)
            y = randint(1, 10)
            if check_possibility_function((x, y), ships):
                ships.append((x, y))
                is_placed = True
                break
        if not is_placed:
            break
    if len(ships) == number_of_ships:
        return ships
    return None


def check_location_possibility(ship, ships):
    for x in range(max(ship[0] - 1, 0), min(ship[0] + 1, 10) + 1):
        for y in range(max(ship[1] - 1, 0), min(ship[1] + 1, 10) + 1):
            if (x, y) in ships:
                return False
    return True


def check_location_possibility_in_chessboard_order(ship, ships):
    locations_to_check = {
        (max(ship[0] - 1, 0), ship[1]),               # left
        (max(ship[0] + 1, 0), ship[1]),               # right
        (ship[0],             min(ship[1] - 1, 10)),  # top
        (ship[0],             min(ship[1] + 1, 10)),  # bottom
        (ship[0],             ship[1]),               # center
    }
    for location in locations_to_check:
        if location in ships:
            return False
    return True


def retry():
    print("Не удалось расположить корабли в этот раз =(. Попробуйте изменить количество кораблей")
    answer = None
    while answer not in ['Д', 'д', 'Н', 'н', '']:
        answer = input("Попробовать еще раз? [Д/н]")
    return answer not in ['Н', 'н']


def main():
    print('Типы расположения кораблей:')
    print(' 1 - Морской бой, по порядку')
    print(' 2 - Шахматный, по порядку')
    print(' 3 - Морской бой, случайный')
    print(' 4 - Шахматный, случайный')
    ships_location_type = int(input("Выберите тип расположения кораблей [1-4] иначе 4 -> "))
    number_of_ships = int(input('Введите количество кораблей для расположения на поле 10 х 10 -> '))

    while True:
        if ships_location_type == 1:
            ships = place_ships(number_of_ships)
        elif ships_location_type == 2:
            ships = place_ships_in_chessboard_order(number_of_ships)
        elif ships_location_type == 3:
            ships = random_place_ships(number_of_ships, check_location_possibility)
        else:
            ships = random_place_ships(number_of_ships, check_location_possibility_in_chessboard_order)

        if ships:
            break
        if not retry():
            exit()

    print()
    print_map(ships)
    print()
    print_cords(ships)


if __name__ == '__main__':
    main()



